#include <iostream>
#include <conio.h>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

struct MadLib {
	string Adjective;
	string Noun;
	string Person;
	string Food;
	string BaseballTeam;
	string FavColor;
	string FavCar;
	string FavCity;
	string FavDrink;
	string FavClass;
};

int main() {
	char active = 'y';
	char save;
	string path = "D:\\C++ Code\\MadLib.text";

	MadLib Answers;
	vector<MadLib> Response;

	while (active == 'y') {

		cout << "\n" << "Enter An Adjective (Describing Word): ";
		cin >> Answers.Adjective;

		cout << "Enter A Noun: ";
		cin >> Answers.Noun;

		cout << "Enter A Name Of A Person: ";
		cin >> Answers.Person;

		cout << "Enter Your Favorite Food: ";
		cin >> Answers.Food;

		cout << "Enter Your Favorite Baseball Team: ";
		cin >> Answers.BaseballTeam;

		cout << "Enter Your Favorite Color: ";
		cin >> Answers.FavColor;

		cout << "Enter Your Favorite Car: ";
		cin >> Answers.FavCar;

		cout << "Enter Your Favorite City: ";
		cin >> Answers.FavCity;

		cout << "Enter Your Favorite Drink: ";
		cin >> Answers.FavDrink;

		cout << "Enter Your Favorite Class: ";
		cin >> Answers.FavClass;

		Response.push_back(Answers);

		cout << "\n" << "One day my " << Answers.Adjective << " and I decided to go to go find a " << Answers.Noun;
		cout << "\n" << "Their name is " << Answers.Person << " and they love to eat " << Answers.Food;
		cout << "\n" << "We love to watch the " << Answers.BaseballTeam << " play at home." << " I like how their jerseys have " << Answers.FavColor;
		cout << "\n" << "We drove home in my " << Answers.FavCar << " and we drove to " << Answers.FavCity << " to drink some " << Answers.FavDrink;
		cout << "\n" << "We had so much fun we were almost late to " << Answers.FavClass << " the next day. The End." << "\n";

		cout << "\n" << "Would You Like To Save To A File? (Y/N) ";
		cin >> save;

		if (save == 'y') {
			ofstream ofs;
			ofs.open(path);

			ofs << "\n" << "One day my " << Answers.Adjective << " friend " << Answers.Person << " and I decided to go to go find a " << Answers.Noun <<
				"\n" << "We love to eat " << Answers.Food <<
				"\n" << "We love to watch the " << Answers.BaseballTeam << " play at home." << " I like how their jerseys have " << Answers.FavColor <<
				"\n" << "We drove in my " << Answers.FavCar << " all the way to " << Answers.FavCity << " to drink some " << Answers.FavDrink <<
				"\n" << "We had so much fun we were almost late to " << Answers.FavClass << " the next day. The End.";

			ofs.close();

			cout << "\n" << "Would You Like To Play Again? ";
			cin >> active;
		}

		else {
			cout << "\n" << "Thanks For Playing!";
			active = 'n';
		}
	}

	_getch();
	return 0;
}